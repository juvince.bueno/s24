// Task #2

db.users.find(
	{ $or: [{firstName: {$regex:'s', $options: '$i'}
			}, 
			{lastName:{$regex: 'd', $options: '$i'}

				}]
			
	},
	{
		firstName: 1,
		lastName: 1,
         _id: 0
	}
)

// Task #3

db.users.find({ $and: [{department: "HR"}, {age: {$gt: 70}}]})

// Task #4

db.users.find(
	{$and:[
		{firstName: {$regex:'e', $options: '$i'}},
		{age: {$lt: 30}}
		]
	}
)